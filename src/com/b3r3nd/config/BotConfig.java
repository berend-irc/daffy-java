package com.b3r3nd.config;

public interface BotConfig {
	
    public static final String TRIGGER = "$";
    public static final String SERVER = "irc.onlinegamesnet.net";
    public static final int PORT = 6667;
    public static final String PASS = "";
    public static final String DEV_CHAN = "#Daffy";
    public static final String NAME = "Doffy";
    public static final String DMODE = "";
}
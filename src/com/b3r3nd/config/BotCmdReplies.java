package com.b3r3nd.config;

public interface BotCmdReplies {

	public static final String ACCOUNT = "Daffy";
	public static final String PASSWORD = "";
	
	// Invalid Parameters
	public static final String INV_PAR = "Invalid Parameters";
	public static final String INV_PAR_JOIN = "Usage: Join <#channelname> <key>";
	public static final String INV_PAR_SAY = "Usage: Say <Message>";
	public static final String INV_PAR_OP = "Usage: Op <User>";
	public static final String INV_PAR_VOICE = "Usage: Voice <User>";
	
	// Correct usage
	public static final String COR_JOIN = "Joined channel ";
	public static final String COR_PART = "Parted channel ";
	public static final String COR_SAY = "Said ";
	public static final String COR_OP = "Given operator status to ";
	public static final String COR_DEOP = "Removed operator status from ";
	public static final String COR_VOICE = "Given voice to ";
	public static final String COR_DEVOICE = "Removed voice from ";
	
	// Other Command replies
	public static final String TEST = "Test Cmd";
}

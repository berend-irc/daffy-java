package com.b3r3nd.ircbot.data;

import java.io.*;

public class OutputThread extends Thread {
    
    OutputThread(MainBot bot, Queue outQueue) {
        _bot = bot;
        _outQueue = outQueue;
        this.setName(this.getClass() + "-Thread");
    }
    static void sendRawLine(MainBot bot, BufferedWriter bwriter, String line) {
        if (line.length() > bot.getMaxLineLength() - 2) {
            line = line.substring(0, bot.getMaxLineLength() - 2);
        }
        synchronized(bwriter) {
            try {
                bwriter.write(line + "\r\n");
                bwriter.flush();
                bot.log(">>>" + line);
            }
            catch (Exception e) {
            }
        }
    }
    
    public void run() {
        try {
            boolean running = true;
            while (running) {
                Thread.sleep(_bot.getMessageDelay());
                
                String line = (String) _outQueue.next();
                if (line != null) {
                    _bot.sendRawLine(line);
                }
                else {
                    running = false;
                }
            }
        }
        catch (InterruptedException e) {
        }
    }
    
    private MainBot _bot = null;
    private Queue _outQueue = null;
    
}

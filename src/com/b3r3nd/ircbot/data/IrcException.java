package com.b3r3nd.ircbot.data;

@SuppressWarnings("serial")
public class IrcException extends Exception {
    public IrcException(String e) {
        super(e);
    }
    
}

package com.b3r3nd.ircbot.data;

@SuppressWarnings("serial")
public class NickAlreadyInUseException extends IrcException {
 
    public NickAlreadyInUseException(String e) {
        super(e);
    }
    
}

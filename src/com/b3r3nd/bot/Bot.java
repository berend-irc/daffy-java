package com.b3r3nd.bot;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.b3r3nd.admin.Admin;
import com.b3r3nd.config.BotConfig;
import com.b3r3nd.config.BotCmdReplies;
import com.b3r3nd.ircbot.data.IrcException;
import com.b3r3nd.ircbot.data.NickAlreadyInUseException;
import com.b3r3nd.ircbot.data.MainBot;

/**
 * 
 * Java IRC Bot
 * @author Berend de Groot
 * @version 1.0
 * 
 */

public class Bot extends MainBot implements BotCmdReplies, BotConfig {
	
    public Bot() {
    }
	
	public static void main(String[] args) throws NickAlreadyInUseException, IOException, IrcException {
		Bot bot = new Bot();
    	bot.setName(NAME);
     	bot.setLogin(NAME);
     	bot.connect(SERVER, PORT, PASS);
	}
	
	public void onConnect() {
		joinChannel(DEV_CHAN);
		//sendNotice("Authserv", "auth " + ACCOUNT + " " + PASSWORD);
	}
	
	 public void onMessage(String channel, String sender, String login, String hostname, String message) {
		 /*
		  * String msg			=		all arguments except command.
		  * List args			=		all arguments including command.
		  * List cargs			=		all arguments except command.
		  * boolean isAdmin 	=		True if Admin.
		  */
		 List<String> args = Arrays.asList(message.split("\\s"));
		 String cmd = args.get(0);
		 String msg = "";
		 for (String s : args)
		 {
			 if(!s.contains(TRIGGER)) {
				 msg += s + "\t";
			 }
		 }
		 List<String> cargs = Arrays.asList(msg.split("\\s"));
		 boolean isAdmin = Admin.IsAdmin(hostname);
		 
			
		 /*
		  * Parameters	:		1+.
		  * Action		:		Say text.
		  */
		 if (cmd.toLowerCase().equals(TRIGGER + "say")) {
			 if(args.size() > 1) {
				 sendMessage(channel, msg);
			}
			 else {
				 sendNotice(sender, INV_PAR_SAY);
			 }
		 }
	    	
		 /*
		  * Parameters	:		1/2.
		  * Action		:		Join Channel.
		  * Extra		:		If 2nd argument is given, use as key.
		  */
	    if (cmd.toLowerCase().equals(TRIGGER + "join")) {
	    	if(args.size() > 2) {
	    		this.joinChannel(args.get(1), args.get(2));
	    		sendNotice(sender, COR_JOIN + msg);
	    	}
	    	else if(args.size() > 1) {
		    	this.joinChannel(args.get(1));
		    	sendNotice(sender, COR_JOIN + msg);
		    	}
	    	else {
	    		sendNotice(sender, INV_PAR_JOIN);
	    	}
	    }
	
	    /*
	     * Parameters	:		0/1.
	     * Action		:		Part Channel.
	     * Extra		:		Part current channel when no parameter is given.
	     */
	    if (cmd.toLowerCase().equals(TRIGGER + "part")) {
	    	if(args.size() > 1) {
	    		this.partChannel(args.get(1));
	    		sendNotice(sender, COR_PART + msg);
	    	}
	    	else {
	    		this.partChannel(channel);
	    		sendNotice(sender, COR_PART + channel);
    		}
	    }
		
		/*
		 * Parameters	:		none.
		 * Action		: 		Check If Admin.
		 */
		if (cmd.toLowerCase().equals(TRIGGER + "check")) {
			if(isAdmin == true) {
				sendMessage(channel, "Admin");	
			}
			else {
				sendMessage(channel, "Noob");
			}
		}
		
		/*
		 * Parameters	:		none.
		 * Action		:		Displays Version.
		 */
	   	if (cmd.toLowerCase().equals(TRIGGER + "version")) {
	   		sendMessage(channel, sender + ": Daffy " + VERSION + " Java IRC Bot");
	   		sendMessage(channel, sender + ": Coded by berend");
	   	}
	   	
	   	/*
	   	 * Parameters	:		1+.
	   	 * Action		:		Give Operator to user(s).
	   	 */
	   	if (cmd.toLowerCase().equals(TRIGGER + "op")) {
	   		if(args.size() > 1) {
	   			for(String o: cargs) {
	   				this.op(channel, o);
	   			}
	   			sendNotice(sender, COR_OP + msg);
	   		}
	   		else {
	   			sendNotice(sender, INV_PAR_OP);
	   		}
	   	}
	   	
	   	/*
	   	 * Parameters	:		1+.
	   	 * Action		:		Removes Operator From user(s).
	   	 */
	   	if (cmd.toLowerCase().equals(TRIGGER + "deop")) {
	   		if(args.size() > 1) {
	   			for(String o: cargs) {
	   				this.deOp(channel, o);
	   			}
	   			sendNotice(sender, COR_DEOP + msg);
	   		}
	   		else {
	   			sendNotice(sender, INV_PAR_OP);
	   		}
	   	}
	   	
	   	/*
	   	 * Parameters	:		1+.
	   	 * Action		:		Gives voice to an user.
	   	 */
	   	if (cmd.toLowerCase().equals(TRIGGER + "voice")) {
	   		if(args.size() > 1) {
	   			for(String v: cargs) {
	   				this.voice(channel, v);
	   			}
	   			sendNotice(sender, COR_VOICE + msg);
	   		}
	   		else {
	   			sendNotice(sender, INV_PAR_VOICE);
	   		}
	   	}
	   	
	   	/*
	   	 * Parameters	:		1+.
	   	 * Action		:		Gives voice to an user.
	   	 */
	   	if (cmd.toLowerCase().equals(TRIGGER + "devoice")) {
	   		if(args.size() > 1) {
	   			for(String v: cargs) {
	   				this.deVoice(channel, v);
	   			}
	   			sendNotice(sender, COR_DEVOICE + msg);
	   		}
	   		else {
	   			sendNotice(sender, INV_PAR_VOICE);
	   		}
	   	}
	 }
}
